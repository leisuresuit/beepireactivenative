import React from 'react';
import { View, Text, Image, TouchableOpacity, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';

const CarDetail = ({ car }) => {
  const {
    title,
    formattedSalePrice,
    formattedMileage,
    carShotUrls,
    carPageUrl
 } = car;
  const {
    imageStyle,
    footerContentStyle,
    titleContentStyle,
    titleTextStyle,
    priceTextStyle,
    mileageTextStyle
  } = styles;

  return (
    <TouchableOpacity onPress={() => Linking.openURL('https://www.beepi.com'.concat(carPageUrl))}>
      <Card>
        <Image
          style={imageStyle}
          source={{ uri: 'http:'.concat(carShotUrls.heroShotUrl) }}
        />
        <CardSection>
          <View style={footerContentStyle}>
            <View style={titleContentStyle}>
              <Text style={titleTextStyle}>{title}</Text>
              <Text style={mileageTextStyle}>{formattedMileage} mi.</Text>
            </View>
          </View>
          <Text style={priceTextStyle}>{formattedSalePrice}</Text>
        </CardSection>
      </Card>
    </TouchableOpacity>
  );
};

const styles = {
  imageStyle: {
    height: 250,
    width: null,
    flex: 1
  },
  titleTextStyle: {
    alignSelf: 'flex-start',
    fontSize: 16,
    fontWeight: '600'
  },
  mileageTextStyle: {
    alignSelf: 'flex-start',
    fontSize: 12
  },
  priceTextStyle: {
    alignSelf: 'flex-end',
    color: '#009bff',
    fontSize: 16,
    fontWeight: '600'
  },
  footerContentStyle: {
    flexDirection: 'row',
    width: null,
    flex: 1,
  },
  titleContentStyle: {
    flexDirection: 'column'
  },
};

export default CarDetail;
