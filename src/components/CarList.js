import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';
import CarDetail from './CarDetail';

class CarList extends Component {
  state = { cars: [] };

  componentWillMount() {
    axios.post('https://www.beepi.com/v1/listings/carsSearch', {})
      .then(response => this.setState({ cars: response.data.carsOnSale }))
      .catch(error => console.log(error));
  }

  renderCars() {
    console.log(this.state.cars);
    return this.state.cars.map(car =>
      <CarDetail key={car.vin} car={car} />
    );
  }

  render() {
    return (
      <ScrollView>
        {this.renderCars()}
      </ScrollView>
    );
  }
}

export default CarList;
