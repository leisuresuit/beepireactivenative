import React from 'react';
import { View, Text } from 'react-native';

const Header = (props) => {
  const {
    containerStyle,
    textStyle
  } = styles;

  return (
    <View style={containerStyle}>
      <Text style={textStyle}>{props.headerText}</Text>
    </View>
  );
};

const styles = {
  containerStyle: {
    backgroundColor: '#009bff',
    justifyContent: 'center',
    height: 50,
    paddingLeft: 15,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    color: '#ffffff',
    fontSize: 20,
    fontWeight: '600'
  }
};

export default Header;
