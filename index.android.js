/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { AppRegistry, View } from 'react-native';
import CarList from './src/components/CarList';
import Header from './src/components/Header';

const helloworld = () => (
  <View style={{ flex: 1 }}>
    <Header headerText={'Cars for Sale'} />
    <CarList />
  </View>
);

AppRegistry.registerComponent('helloworld', () => helloworld);
